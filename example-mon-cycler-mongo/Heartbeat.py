"""A class for a hearbeat daemon that updates the DB with stats on every beat"""

import threading
import time
import datetime
import logging

class Heartbeat(object):
    """A class for a hearbeat daemon that updates the DB with stats on every beat"""

    __logger = logging.getLogger()

#pylint: disable-msg=too-many-arguments
    def __init__(self, my_interval, my_coll, 
                 cycState0=0,cycCnt0=0, verbose=False):
        self.__my_interval = my_interval
        self.__verbose = verbose
        self.__db_coll = my_coll
        self.__beat_my_heart = True

        self.__init_stats()
        self.cyclerState=cycState0
        self.cyclerCounter=cycCnt0
        self.myCounter=0

        heartbeat_thread = threading.Thread(target=self.__heartbeat)
        heartbeat_thread.setDaemon(True)
        heartbeat_thread.start()
#pylint: enable-msg=too-many-arguments

    def __init_stats(self):
        """Intialize stats from DB latest record"""

        self.__logger.info("Init from DB-coll: %s, nRec=%d"%(self.__db_coll,self.__db_coll.count()))

        if self.__db_coll.count()>0:
            last_doc = self.__db_coll.find().skip(self.__db_coll.count()-1)[0]
            self.cycler = last_doc['cycler']
            self.__print_stats()


    def __heartbeat(self):
        """Send a heartbeat to DB """

        time.sleep(self.__my_interval)
        while self.__beat_my_heart:
            self.cycler={'state':self.cyclerState,'counter':self.cyclerCounter}
            entry = {'cycler': self.cycler,
                     'date' : datetime.datetime.utcnow()}
            self.__db_coll.insert(entry)

            if self.__verbose:
                self.__print_stats()

            time.sleep(self.__my_interval)

    def __print_stats(self):
        self.__logger.info("DB nRec=%d"%(self.__db_coll.count()))
        for key in self.cycler.keys():
            self.__logger.info("<%s> last = %i", key.replace('_', ' '), self.cycler[key])
