"""
Special copy for Jan Balewski
Utility class to handle connections to mongoDb and fetching collections
"""

import os
import base64
import pymongo
from pymongo import MongoClient

__author__ = "Mustafa Mustafa,Jan Balewski"
__email__ = "balewski@lbl.gov"

import sys
(va,vb,vc,vd,ve)=sys.version_info
assert(va==3)  # needes Python3

class MongoDbUtil(object):
    """Usage e.g.
       m = mongoDbUtil('admin')
       coll = m.db['myCollection']"""

    def __init__(self, user='roadOnly'):

        self.__db_name = os.getenv('myMongoDb_name', 'FALSE3')
        self.__db_server =os.getenv('myMongoDb_server', 'FALSE4')
        print ('dbN=',self.__db_name)  
        
        if user == 'admin':
            self.__user = self.__db_name+'_admin'
            self.__password = os.getenv('myMongoDb_admin_pass', 'FALSE')
        else:
            self.__user = self.__db_name+'_ro'
            self.__password = os.getenv('myMongoDb_ro_pass', 'FALSE')

        if self.__password == 'FALSE':
            print ("Password for user %s is not available"%self.__user)
            exit(1)

        print ("DB_CNT" , self.__user, self.__password, self.__db_name ,self.__db_server)
        self.__connect_db()

    def __connect_db(self):
        """Establish connection to DB
        """
        
        print ("Connecting to %s@%s. User: %s ..."%(self.__db_server, self.__db_server, self.__user))
        connection_timeout_max = 5
        xxx='mongodb://{0}:{1}@{2}/{3}'.format(self.__user, self.__password, self.__db_server, self.__db_name)
        
        print ('DB_CNT-CMD',xxx)
        try:
#            self.__client = MongoClient('mongodb://{0}:{1}@{2}/{3}'.format(self.__user, str(base64.b64decode(self.__password).decode('utf-8')), self.__db_server, self.__db_name),
            self.__client = MongoClient(xxx)
            del self.__password
            self.__client.server_info() # attempt a connection
            self.__database = self.__client[self.__db_name]

        except pymongo.errors.ServerSelectionTimeoutError:
            print ("ERROR: Could not connect to DB server ...")
            exit(1)

    def database(self):
        """Return DB"""
        return self.__database
