#!/usr/bin/env python

"""Daemon, do periodic , heavy duty work with random delays, reports to own db-cllection"""

import sys
import time
import datetime
import logging
import load_configuration
import pymongo
from MongoDbUtilLean2 import MongoDbUtil
from Heartbeat import Heartbeat

(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

__author__ = "Mustafa Mustafa,Jan Balewski"
__email__ = "balewski@lbl.gov"

# pylint: disable=C0103
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)-15s - %(levelname)s - %(module)s : %(message)s')
__logger = logging.getLogger(__name__)
# pylint: enable=C0103


def main():
    """To be used in CLI mode"""
    logging.info("CLI-start")
    args = load_configuration.get_args(__doc__)
    cycler(args.configuration)

def cycler(config_file):
    config = load_configuration.load_configuration(config_file)
    database = MongoDbUtil('admin').database()
    myColl=database[config['cycler_coll']]
    logging.info("Init from DB-coll: %s, nRec=%d"%(myColl,myColl.count()))

    heartbeat = Heartbeat(config['heartbeat_interval'],
                          database[config['heartbeat_coll']],
                          verbose=True)
    logging.info("Heartbeat thread spawned")
    
    # main work - loop over and do some stuff
    heartbeat.cyclerState=0
    time.sleep(5)
    while True:        
        heartbeat.cyclerState=1
        heartbeat.cyclerCounter+=1
        # fake data collection is here ...
        time.sleep(15)
        heartbeat.cyclerState=2
        # assembling and writing payload to DB
        logging.info("myCounter %d, DB nRec=%d"%(heartbeat.cyclerCounter,myColl.count()))
        outData={'nodeName':'abc-node', 'load':10*heartbeat.cyclerCounter}
        entry = {'payload': outData, 'counter':heartbeat.cyclerCounter,
                 'date' : datetime.datetime.utcnow()}
        myColl.insert(entry)
        # all is done, now wait for another cycle
        heartbeat.cyclerState=2
        time.sleep(config['cycler_sleep_interval'])

if __name__ == '__main__':
    main()
