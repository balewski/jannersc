#!/usr/bin/env python
""" Example use of mongoDB swiss knife """

from MongoDbUtilLean1 import MongoDbUtil

def main(collName='janCollC' ):
    dbHead = MongoDbUtil('ro').database()
#    dbHead = MongoDbUtil('admin').database()
    print ('list all collections from DB=',dbHead)
 
    cols = dbHead.collection_names()
    for cx in cols:
        print (cx)

    print ('read from  coll=',collName)
    dbColl = dbHead[collName]
    print ('dbColl=',dbColl)

    nLim=30
    mxN=dbColl.count()
    print ('size=',mxN, ' print up to ',nLim)
    
    for it in dbColl.find().limit(nLim):
        print (it)

if __name__ == '__main__':
    main()
