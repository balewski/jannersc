#!/usr/bin/env python

""" pulls  records for last K minutes from DB  & reports """
import sys
import time
import datetime
import pprint
import logging
import load_configuration
import pymongo
from MongoDbUtilLean2 import MongoDbUtil

(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3

__author__ = "Jan Balewski"
__email__ = "balewski@lbl.gov"

# pylint: disable=C0103
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format='%(asctime)-15s - %(levelname)s - %(module)s : %(message)s')
__logger = logging.getLogger(__name__)
# pylint: enable=C0103


def main():
    """To be used in CLI mode"""
    print("CLI-reporter: doc=",__doc__,"=")
    args = load_configuration.get_args(__doc__)
    reporter(args.configuration)

def reporter(config_file):
    config = load_configuration.load_configuration(config_file)
    database = MongoDbUtil('admin').database()
    heartColl=database[config['heartbeat_coll']]
    cycColl=database[config['cycler_coll']]
    logging.info("Init from DB-coll: %s, nRec=%d"%(heartColl,heartColl.count()))
    logging.info("Init from DB-coll: %s, nRec=%d"%(cycColl,cycColl.count()))
    
    # main work - loop over and do some stuff
    myCounter=0
    while True:        
        # reading  payload from  DB
        logging.info("myCounter %d, DB nRec heart=%d cycler=%d"%(myCounter,heartColl.count(),cycColl.count()))
        if heartColl.count()>0:
            lastHeart=heartColl.find().skip(heartColl.count()-1)[0]
            print(lastHeart)
        if cycColl.count()>0:
            lastCyc=cycColl.find().skip(cycColl.count()-1)[0]
            print(lastCyc)

        break # tmp run just one time
        time.sleep(config['reporter_sleep_interval'])

if __name__ == '__main__':
    main()
