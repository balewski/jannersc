#!/usr/bin/env python

""" 
Insert single record to mongoDb
Needed packages:
module load python/2.7.3  

"""
import pymongo
from pymongo import MongoClient

URI ='mongodb://mongodb03.nersc.gov:27017'
DB_NAME='PdsfArchive' # aka colsdlection
USERNAME='PdsfArchive_admin'
PASSWORD='w2e23sddf21'
cli = MongoClient(URI)
dbh = cli[DB_NAME]
print 'my db host=',dbh,' , client=',cli

dbh.authenticate(USERNAME, PASSWORD)
print 'ok-auth'
# mongodb://[username:password@]host1[:port1][,host2[:port2],...[,hostN[:portN]]][/[database][?options]]

cli2 = MongoClient('mongodb://PdsfArchive_admin:w2e23sddf21@mongodb03.nersc.gov/PdsfArchive') #,serverSelectionTimeoutMS=5)
print 'client2=',cli2


collName='authorCollA'
print 'write to coll=',collName
 
# - - - -  assemble record to be inserted
import datetime

# the name of collection is 'name'
postOne={"name" : "Canada3","date": datetime.datetime.utcnow()}

print 'my new record to be added=',postOne

# ---- insert record into collection
postMan = dbh.posts
print 'my postMan so far=',postMan

id=postMan.insert([postOne])
print 'id=',id, ' wr-ok'
