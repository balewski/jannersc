#!/usr/bin/env python
""" Example of write to  mongoDB w/ swiss knife """

import datetime
import sys,os
import json, pprint
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
pp = pprint.PrettyPrinter(indent=4)

__author__ = "Jan Balewski"
__email__ = "balewski@lbl.gov"
    
from MongoDbUtilLean1 import MongoDbUtil

#---------------------
def listAllCollections1(collName):
    dbHead = MongoDbUtil('xxx').database()
  
    cols = dbHead.collection_names()
    for c in cols:
        print (c)

    #col = raw_input('Input a collection from the list above to show its field names: ')
    colN=collName

    collection = dbHead[colN].find()

    keylist = []
    for item in collection:
         for key in item.keys():
             if key not in keylist:
                 keylist.append(key)
             if isinstance(item[key], dict):
                 for subkey in item[key]:
                     print ('A-subkey:',subkey, ' in item=',item)
                     subkey_annotated = key + "." + subkey
                     if subkey_annotated not in keylist:
                         keylist.append(subkey_annotated)
                         if isinstance(item[key][subkey], dict):
                             for subkey2 in item[subkey]:
                                 subkey2_annotated = subkey_annotated + "." + subkey2
                                 if subkey2_annotated not in keylist:
                                     keylist.append(subkey2_annotated)
             if isinstance(item[key], list):
                 for l in item[key]:
                     if isinstance(l, dict):
                         for lkey in l.keys():
                             lkey_annotated = key + ".[" + lkey + "]"
                             if lkey_annotated not in keylist:
                                 keylist.append(lkey_annotated)
    keylist.sort()
    print ("sorted keylist:",keylist)
    for key in keylist:
        keycnt = dbHead[col].find({key:{'$exists':1}}).count()
        print ("%-5d\t%s" % (keycnt, key))


#---------------------
def listAllCollections2():
    dbObj=MongoDbUtil('xx')    
    dbHead = dbObj.database()
    collection = dbHead.collection_names(include_system_collections=False)
    for collX in collection:
        print ('coll=',collX)

#---------------------
def recentRead(collName,nShow=5):

    print ('read from  coll=',collName)
    dbColl = MongoDbUtil('reader').database()[collName]
    pp.pprint (dbColl)
    
    mxN=dbColl.count()
    if mxN<=0:
        print ('your collection is empty, quit')
        return
    
    if nShow > mxN-1:
        recL=dbColl.find()
    else:
        recL=dbColl.find().skip(mxN-nShow)

    print ('show  last ',nShow, ' records of ',mxN)
    print
    k=0
    for it in recL:
        if k==0:
            print('1st captured record:')
            pp.pprint (it)
        k+=1    
        print (mxN-nShow+k,  'utc=',it['date'],' nHost=',it['nHost'])



################################
#     MAIN
################################


if __name__ == '__main__':

    collName='pdsfNabuseA'
    collName='x1-xrdStarPdsfA'

    recentRead(collName)
    #listAllCollections1(collName)  # works partially
    #listAllCollections2()  # only collection names, all collections
