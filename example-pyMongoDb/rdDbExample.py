#!/usr/bin/env python
""" Example of read from  mongoDB w/ swiss knife """
import sys,os
import json, pprint
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
pp = pprint.PrettyPrinter(indent=4)

__author__ = "Jan Balewski"
__email__ = "balewski@lbl.gov"

import datetime    
# new Mustafa mongoDB I/O
sys.path.append(os.path.abspath("../"))
from MongoDbUtilLean1 import MongoDbUtil

#---------------------
def recentRead(collName,nShow=5):

    print ('read from  coll=',collName)
    dbColl = MongoDbUtil('reader').database()[collName]
    pp.pprint (dbColl)
    
    mxN=dbColl.count()
    if mxN<=0:
        print ('your collection is empty, quit')
        return
    
    if nShow > mxN-1:
        recL=dbColl.find()
    else:
        recL=dbColl.find().skip(mxN-nShow)

    print ('show  last ',nShow, ' records of ',mxN)
    print
    k=0
    for it in recL:
        if k==0:
            pp.pprint (it)
        k+=1    
        print (mxN-nShow+k,  'utc=',it['date'], ' nBad=',it['nBad'],' nHost=',it['nHost'],' sumW=',it['sumW'])



################################
#     MAIN
################################

if __name__ == '__main__':

    collName='x-tmp-posts'
    recentRead(collName)
