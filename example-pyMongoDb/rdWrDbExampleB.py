#!/usr/bin/env python
""" Example of write to  mongoDB w/ swiss knife """

import datetime
import sys,os
import json, pprint
(va,vb,vc,vd,ve)=sys.version_info ; assert(va==3)  # needes Python3
pp = pprint.PrettyPrinter(indent=4)

__author__ = "Jan Balewski"
__email__ = "balewski@lbl.gov"

    
from MongoDbUtilLean1 import MongoDbUtil

def oneWrite(collName,userD):

    print ('write to coll=',collName)
    dbColl = MongoDbUtil('admin').database()[collName]
    print (dbColl)
    mxN=dbColl.count()

    postOne={}
    postOne['date']=datetime.datetime.utcnow()
    postOne['users']=userD
    postOne['nHost']=123+10*mxN
   

    
    # ---- insert record into collection
    print ('writte rec=',mxN+1,' body=',postOne )
    id=dbColl.insert([postOne])
    print ('id=',id)



################################
#     MAIN
################################


if __name__ == '__main__':

    collName='x1-xrdStarPdsfA'
    userD={"jan3":51,"tom2":43}
    oneWrite(collName,userD)

