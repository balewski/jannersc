#!/bin/bash
name1=calico-1a
myName=`hostname`
ipDomain=192.168.0

function launchWorkers {
    if [[ $myName == *"$name1"* ]] ; then
	echo "cal-1 workers ------------------"
	docker run --net=none --name workload-A -tid busybox
	docker run --net=none --name workload-B -tid busybox
	docker run --net=none --name workload-C -tid busybox
	docker ps
    else
	echo "cal-2 doc-runX"
    fi
}


function launchProfiles {
    if [[ $myName == *"$name1"* ]] ; then
	echo "cal-1 profiles -------------------"
	calicoctl profile add PROF_A_C_E
	calicoctl profile add PROF_B
	calicoctl profile add PROF_D
	calicoctl profile show --detailed
    else
	echo "cal-2 profilesX"
    fi
}

function addCalicoEndpoints {
    if [[ $myName == *"$name1"* ]] ; then
        echo "cal-1 addCalicoEndpoints -------------------"
	sudo calicoctl container add workload-A $ipDomain.1
	sudo calicoctl container add workload-B $ipDomain.2
	sudo calicoctl container add workload-C $ipDomain.3
    else
        echo "cal-2 endpointsX"
    fi
}



function connectWorkers {
     sudo calicoctl node 
    if [[ $myName == *"$name1"* ]] ; then
	echo "cal-1 connect"
	calicoctl container workload-A profile append PROF_A_C_E
	calicoctl container workload-B profile append PROF_B
	calicoctl container workload-C profile append PROF_A_C_E
	calicoctl profile show --detailed
    else
	echo "cal-2 connectX"
    fi
}



#=================
#  M A I N 
#================

whoami|grep core

if [ $? -ne 0  ]; then
    echo incorrect user, change to u=core
    echo sudo -u core -i
    exit
fi

echo this is ip of Calico master:
env | grep ETCD_AUTHORITY

docker ps |grep workload
if [ $? -ne 0  ]; then
    echo  no test-workload boxes, launch them
    launchWorkers
fi
#exit

calicoctl profile show --detailed |grep PROF
if [ $? -ne 0  ]; then
    echo  no calio-profiles, launch them
    launchProfiles
fi
#exit

sudo calicoctl container workload-B endpoint show |grep active
if [ $? -ne 0  ]; then
    echo  no CalicoEndpoints, add them
    addCalicoEndpoints
fi
#exit



calicoctl profile show --detailed |grep active
if [ $? -ne 0  ]; then
    echo  no network between workers, connect them
    connectWorkers
fi




