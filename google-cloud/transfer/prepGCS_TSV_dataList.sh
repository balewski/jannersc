#!/bin/bash
#here  are large files for transfer testing
urlCore=http://portal.nersc.gov/project/star/balewski/dataBank
discPath=/project/projectdirs/star/www/balewski/dataBank
dataExt=data

set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value



unhex (){
    b=0;
    while [ $b -lt ${#1} ];  do
	printf "\\x${1:$b:2}";
        b=$((b += 2));
    done
}

md5sum2bytes (){
    while read -r md5sum file; do
        unhex $md5sum;
    done
}

#.............
function measureOne {
    fname=$1
    fullFN=$discPath/$xx
    echo #measure fname=$fname
    line=`ls -l $fullFN`
    #echo $line
    sizeB=`echo $line | cut -f5 -d\ `
    #echo $sizeB
    line=`md5sum $fullFN `
    md5val=`echo $line | cut -f1 -d\ `
    echo md5val=$md5val=

    md5bas64=`echo $line | md5sum2bytes | base64`
    echo md5bas64:$md5bas64:
}

#=============
#   MAIN  
#=============


nFile=0
sumGB=0
tsvFile=${1-dataList}.gcs.tsv
discTsvFile=$discPath/$tsvFile
echo \#input path=$discPath, tsvFile=$tsvFile
echo TsvHttpData-1.0 >$discTsvFile
for xx in $(ls $discPath ) ; do
    echo  work on $nFile  $xx
    measureOne $xx
    echo -e "$urlCore/$xx\t$sizeB\t$md5bas64" >>$discTsvFile
    nFile=$[ $nFile + 1 ]
    sumGB=$[ $sumGB + $sizeB/1024/1024 ]
    if [ $nFile -gt 330 ] ; then break; fi
    #break
done
chmod a+r $discTsvFile
echo \#totals:  nFile=$nFile sumGB=$sumGB
echo your url= $urlCore/$tsvFile
echo head $discTsvFile
head -n3 $discTsvFile
date

