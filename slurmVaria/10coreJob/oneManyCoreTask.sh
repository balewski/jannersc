#!/bin/bash 
nTask=${1-3}
myExe=vet3.exe
echo "start-one for nTask=$nTask exe=$myExe "`date`

cd /global/homes/b/balewski/janNersc/pdsfVaria/stress-node
outPath=$SCRATCH/test2
k=0
for i in `seq 1 ${nTask}`;  do
    coreN="janFit"_$i
    outName=outXXX_$i
    logF=$outPath/${coreN}.log
    outF=${coreN}.out
    k=$[ $k +1]
    echo submit $k $core
    ./$myExe  --coreName $outF --workPath $outPath -w 12 -T 1 >& ${logF} &    
done
echo "end-A submitted $k tasks  "`date`
sleepT=30

sleep $sleepT
# now need to wait until they are done

nRun=`pgrep $myExe | wc -l`
echo nRun=$nRun
k=0
while true ; do
    k=$[ $k +1]
    nRun=`pgrep $myExe | wc -l`
    echo k=$k  nRun=$nRun
    if [ $nRun -le 0 ] ; then break ; fi
    sleep $sleepT
done
echo "end-B completed $k tasks  "`date`

