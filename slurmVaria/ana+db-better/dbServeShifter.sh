#!/bin/bash 
set -u ;  # exit  if you try to use an uninitialized variable


NN=`hostname -f`
line=`ifconfig | grep 'inet addr:10'`=
myIP=`echo $line | cut -f2 -d: | cut -f1 -d\ `


echo  S-$NN myIP=$myIP   
du -hs /home/balewski/
env|grep  SHIFTER_RUNTIME

if [ ! \( -e "${USR_DB_CNF}" \) ] ; then
     echo "ERROR user cnf file ${USR_DB_CNF} does not exist!" >&2
     exit 1
fi


# keep deamon alive until batch dies

echo S-$NN  starting mysqld_safe 
/usr/bin/mysqld_safe  --defaults-file=/mysqlVault/my.cnf &

echo "#S-$NN started mysqld_safe  "`date` 

ls -l /mysqlVault 

echo S-$NN modify  ${USR_DB_CNF}
#cat  ${USR_DB_CNF}
sed  -i-e "s/<db_server_ip>/$myIP/g"  ${USR_DB_CNF}
echo S-$NN after
cat  ${USR_DB_CNF}


echo  S-$NN   stay alive forever

nSleep=5
sleep $nSleep

totSec=0
while true ; do    
    #ps -ef |grep mysqld
    sleep $nSleep
    #echo -n S-$NN  ask-db
    #mysql -u balewski --socket=/mysqlVault/mysql.sock -pjan -e ' show processlist \G'

    NL=`  mysql -u balewski --socket=/mysqlVault/mysql.sock -pjan -e ' show processlist;' | wc -l`
    NC=$[ $NL - 2 ]
    echo  S-$NN   $totSec sec, nDbProc:   $NC  " "`date`
    totSec=$[ $totSec + $nSleep ]
done
