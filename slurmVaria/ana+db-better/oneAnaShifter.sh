#!/bin/bash 

NN=`hostname -f`
line=`ifconfig | grep 'inet addr:10'`=
myIP=`echo $line | cut -f2 -d: | cut -f1 -d\ `
echo A-$NN myIP=$myIP run one DayaBay job on CRAY

date
whoami
env|grep  SHIFTER_RUNTIME
ls -l /home/balewski/

cd /home/balewski/nuwa-docker/NuWa-trunk/
source setup.sh
cd dybgaudi/DybRelease/cmt
source setup.sh

echo A-$NN  testing DayaBay code setup
# simple environment test
root -b -q

echo A-$NN   test connection to DB-server
head -n2 ~/.my.cnf 
line=`ifconfig | grep 'inet addr:10'`
dbIP=`grep host ~/.my.cnf |  cut -f3 -d\ `
dbPass=`grep password ~/.my.cnf |  cut -f3 -d\ `
echo   A-$NN server dbIP=$dbIP  see dump all DYB DB tables

mysql  -u dayabay -p$dbPass  -h $dbIP -e '   use offline_db;   show tables; '


echo A-$NN  testing  target dir
cd $WRK_DIR
pwd

#  the job
nEve=100
outName=reco.jan.root
inpName=/global/project/projectdirs/dayabay/data/exp/dayabay/2015/daq/Neutrino/1114/daq.Neutrino.0057046.Physics.EH3-Merged.SFO-3._0144.data

echo -n A-$NN  start DYB job nEve=$nEev, outName=$outName $inpName=$inpName " "

date
time nuwa.py --random=off --repack-rpc=on -n $nEve --dbirollback="* = 2015-08-31 00:00:00" @$P14BROOT/share/runReco @$P14BROOT/share/runTags @$P14BROOT/share/runODM @$P14BROOT/share/runFilters -o $outName  $inpName

echo -n A-$NN job finished
date
pwd
ls -lrth reco*
