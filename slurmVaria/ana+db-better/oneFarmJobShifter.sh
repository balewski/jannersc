#!/bin/bash 

inpName=${1-badInp1}
outName=${2-badOut2}

iniSleep=$[ $RANDOM % 32 ]

NN=`hostname -f`
line=`ifconfig | grep 'inet addr:10'`=
myIP=`echo $line | cut -f2 -d: | cut -f1 -d\ `
echo A-$NN myIP=$myIP run one ${ANA_CORE} on CRAY nEve=${NUM_EVE} inpName=$inpName outName=$outName iniSleep=${iniSleep} wrkDir=${WRK_DIR} outDir=${OUT_DIR}
date
whoami
env|grep  SHIFTER_RUNTIME
ls -l /home/balewski/

echo A-$NN print DB-server info
head -n2 ~/.my.cnf 

dbIP=`grep host ~/.my.cnf |  cut -f3 -d\ `
dbPass=`grep password ~/.my.cnf |  cut -f3 -d\ `
echo   A-$NN server dbIP=$dbIP  see dump all DYB DB tables
mysql  -u dayabay -p$dbPass  -h $dbIP -e '   use offline_db;   show tables; ' |head 
#echo  A-$NN check load on DB server
#mysql  -u dayabay -p$dbPass  -h $dbIP -e '  show processlist;'

cd /home/balewski/nuwa-docker/NuWa-trunk/
source setup.sh
cd dybgaudi/DybRelease/cmt
source setup.sh

echo A-$NN  testing DayaBay code setup
# simple environment test
root -b -q


echo A-$NN  sleep ${iniSleep}
cd ${WRK_DIR}
pwd
sleep ${iniSleep}
echo -n A-$NN  start DYB job nEve=$nEev, outName=$outName $inpName=$inpName " "
date

time nuwa.py --random=off --repack-rpc=on -n ${NUM_EVE} --dbirollback="* = 2015-08-31 00:00:00" @$P14BROOT/share/runReco @$P14BROOT/share/runTags @$P14BROOT/share/runODM @$P14BROOT/share/runFilters -o $outName  $inpName 

echo -n A-$NN job finished nEve=${NUM_EVE}
date
pwd
ls -lrth $outName
top ibn1 
