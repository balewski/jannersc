#!/bin/bash 
# Note - this script is NOT running in shifter

logF=${1-qaBad1.log}

NN=`hostname -f`
echo Q-$NN start log-to $logF

date
whoami
cd   $STAT_WRK
pwd
# count avaliable hyper-cores
nCpu=`cat /proc/cpuinfo  |grep processor| wc -l`
echo Q-$NN  nHypCpu=$nCpu  and top
top ibn1

# for accounting
module load taskfarmer

echo Q-$NN   test connection to DB-server using ${USR_DB_CNF} 
head -n2 ${USR_DB_CNF}

grep db_server_ip $USR_DB_CNF
if [ $? -eq 0  ]; then 
    echo "Q-$NN  no DB-IP was set, abort"
    exit
fi

dbIP=`grep host ${USR_DB_CNF} |  cut -f3 -d\ `
dbPass=`grep password  ${USR_DB_CNF} |  cut -f3 -d\ `
echo   Q-$NN server dbIP=$dbIP   list DB connections

mysql -u balewski  -pjan -h $dbIP -e ' show processlist;'

nSleep=5
totSec=0
nInpTask=`nl $TASK_LIST |wc -l`
echo "#QAutil_1 taskList:$TASK_LIST len:$nInpTask node0:$NN" >$logF 
echo "#format: date , totSec,  numDbProc, a-cpuW,  nDoneTask, nExeTask " >>$logF 

while true ; do    
    echo  Q-$NN   $totSec sec,  sleep  $nSleep " "`date`
    #ps -ef |grep mysqld
    sleep $nSleep
    
    NL=`  mysql -u balewski -h $dbIP  -pjan -e ' show processlist;' | wc -l`
    NC=$[ $NL - 2 ]

    # 5-minutes load
    w5=`w|head -1 | cut -f5 -d \,`

    # farmer progress

    nExeTask=`cat fastrecovery.${TASK_LIST}.tfin |read_recovery ${TASK_LIST}.tfin |nl | wc -l`
    nDoneTask=`cat progress.${TASK_LIST}.tfin | wc -l`
    echo  Q-$NN myFarmer_update JID=$SLURM_JOBID  task count: inp=$nInpTask done=$nDoneTask exe=$nExeTask "  "`date`


    echo `date` ",  $totSec,  $NC, $w5, $nDoneTask, $nExeTask " >>$logF     
    totSec=$[ $totSec + $nSleep ]
done


echo -n Q-$NN job finished
