#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value


#echo Script name: $0 ,  $# arguments 
if [ $# -ne 1 ]; then
   echo " provide num jobs to submit "
   #exit
fi

nJ=${1-3} 

echo $nJ

for i in `seq 1 $nJ`;
do
    echo submit-$i
    sbatch ./myJobC_pdsf.slr 
done
echo submitted all $nJ ...
sleep 5
./sqs_local 
