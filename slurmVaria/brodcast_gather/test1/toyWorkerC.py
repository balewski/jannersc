#!/usr/bin/env python3

from mpi4py import MPI
import numpy as np
import os

def main():
    os.environ['RANK'] = os.environ['SLURM_PROCID']
    os.environ['WORLD_SIZE'] = os.environ['SLURM_NTASKS']

    # Initialize the MPI environment
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    print('M: MPI myRank=%d of %d'%(rank,size))
    assert size>0 

    nSamp = 20
    data = None

    if rank == 0:
        # Generate data
        data = np.random.uniform(size=(nSamp, 2))
        # Split data into 4 equal blocks
        data_blocks = np.array_split(data, size)
    else:
        data_blocks = None

    # Scatter the data blocks to all ranks
    local_data = comm.scatter(data_blocks, root=0)

    # Perform local computation
    local_prod = local_data[:, 0] * local_data[:, 1]
    print('M: myRank=%d computed'%rank)
    
    # Gather the results back to rank 0
    all_prods = comm.gather(local_prod, root=0)

    if rank == 0:
        # Assemble the results together
        final_prod = np.concatenate(all_prods)
        print("Final product vector:", final_prod)

if __name__ == "__main__":
    main()
