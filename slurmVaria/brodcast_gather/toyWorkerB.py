#!/usr/bin/env python3

from mpi4py import MPI
import numpy as np
from time import time

import argparse
#...!...!..................
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verb",type=int, help="increase debug verbosity", default=1)
    parser.add_argument('-i','--numMegSamp', default=None, type=int, help='num of milion os samples') 
    args = parser.parse_args()
    return args

#...!...!..................
def compute(inpV):  # do complex computations
    nRep=200
    x1=inpV[:,0]
    x2=inpV[:,1]
    # Introduce additional complex computations to increase CPU time
    for _ in range(nRep):  # Loop to increase computational load
        y = np.sin(x1) * np.tanh(x2)
        y = np.tanh(y)+ np.power(x1,x2)
        y = np.where(y > 0, y**2, -y**2)
    return y

def main(numMegSamp, verb=1):
    # Initialize the MPI environment
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    if rank < 2:
        print('M: MPI myRank=%d of %d' % (rank, size))
    assert size > 0

    nSamp = 20 if numMegSamp is None else numMegSamp * 1000000
    data = None

    if nSamp > 30:
        verb = min(verb, 1)
    if rank > 3:
        verb = 0

    if rank == 0:
        T0 = time()
        # Generate data
        data = np.random.uniform(size=(nSamp, 2))
        # Split data into blocks
        data_blocks = np.array_split(data, size)
        print('M: data prep elaT=%.1f sec %s' % (time() - T0, data.dtype))
    else:
        data_blocks = None

    # Scatter the data blocks to all ranks
    local_data = comm.scatter(data_blocks, root=0)

    T0 = time()
    # Perform local computation
    try:
        local_prod = compute(local_data)
    except Exception as e:
        print('Error in compute on rank %d: %s' % (rank, str(e)))
        raise
    tCompute = time() - T0
    if verb:
        print('M: myRank=%d computed, elaT=%.1f sec' % (rank, tCompute))

    # Gather tCompute values to rank 0
    all_tCompute = comm.gather(tCompute, root=0)

    if rank == 0:
        # Calculate and print the average and standard deviation of tCompute
        tCompute_array = np.array(all_tCompute)
        tCompute_mean = np.mean(tCompute_array)
        tCompute_std = np.std(tCompute_array)
        print('numRank=%d  tCompute: %.1f +/- %.1f sec' % (size,tCompute_mean,tCompute_std))

    # Gather the results back to rank 0
    all_prods = comm.gather(local_prod, root=0)
    if rank == 0:
        T0 = time()
        # Assemble the results together
        final_prod = np.concatenate(all_prods)
        if verb > 1:
            print("Final product vector: %s" % final_prod)
        if verb:
            print("Final product vector shape: %s" % str(final_prod.shape))
        print('M: Final gather and assembly elaT=%.1f sec' % (time() - T0))

#=================================
#  M A I N 
#=================================

if __name__ == "__main__":
    args=get_parser()
    try:
        main(args.numMegSamp,args.verb)
    except Exception as e:
        rank = MPI.COMM_WORLD.Get_rank()
        print('Rank %d encountered an error: %s' % (rank, str(e)))
        MPI.COMM_WORLD.Abort()
