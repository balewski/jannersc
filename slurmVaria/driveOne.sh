#!/bin/bash
# needs to be a separate bash script to enroll SLURM-variables properly for each task
runTimeMinutes=$1
outPath=$2
outName=$3
subPath=$4
${subPath}/../pdsfVaria/stress-node/vet3.exe  --writeEventSize 80 --runTime ${runTimeMinutes} --workPath $outPath --coreName ${outName}_${SLURM_PROCID}.out  >& ${outPath}/log.${SLURM_PROCID}
