#!/bin/bash
#cat /proc/cpuinfo
unset AMSDataBaseEnv
ulimit -s 200000000
#export Offline=/global/project/projectdirs/m2814/Offline
export Offline=/global/project/projectdirs/mpccc/balewski/amsTestInp
export fastDir=/mnt
export AMSDataDir=${fastDir}/AMSDataDir
export G4INSTALL=${fastDir}/geant4.10.01
export G4AUTODETECT=NOSET
export AMSProductionLogDir=$HOME/mc/edison/knl
export AMSKILLYOURSELF=1
export ROOTSYS=$Offline/root
export NtupleDir=$SCRATCH/nte
export LogRedirection=1
export ProductionLogDir=$SCRATCH/journals
ulimit -c unlimited
echo inShifter2:`env|grep SHIFTER_RUNTIME`
echo onHostB=`hostname`
cat /etc/*release
du -h /mnt
df -h /mnt
ls -l /mnt

#env|grep SLURM
/usr/bin/time cp $Offline/gg.tgz ${fastDir}
#/usr/bin/time cp $Offline/ad.tgz /mnt
/usr/bin/time tar -C ${fastDir} -zxf ${fastDir}/gg.tgz
#/usr/bin/time tar -C /mnt -zxf /mnt/ad.tgz
du -hs ${fastDir}/*
du -h /mnt
