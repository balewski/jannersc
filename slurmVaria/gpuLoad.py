#!/usr/bin/env python
""" 
evaluate load on Cori-GPU cluster
"""

__author__ = "Jan Balewski"
__email__ = "janstar1122@gmail.com"

import numpy as np
import  time
from  pprint import pprint
import datetime
import subprocess

import argparse
def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-v","--verbosity",type=int,choices=[0, 1, 2],
                        help="increase output verbosity", default=1, dest='verb')

    #parser.add_argument("-s", "--slurmDir", nargs='+',help="path to inputs, a l blank separated list", default=['590283/3'])


    parser.add_argument("-u","--perUser",default=False,action='store_true', help="enable per-user breakdown")
    
    parser.add_argument("-xn","--perNode",default=True,action='store_false', help="disable per-node breakdown")
    
    parser.add_argument("-o", "--outPath",
                        default='out/',help="output path for plots and tables")
 

    args = parser.parse_args()
    for arg in vars(args):  print( 'myArg:',arg, getattr(args, arg))
    assert args.perUser or args.perNode
    return args
#------------------
def memStr2Gb(x0):
    #
    x=x0.lower()
    #print("memStr2Gb INP:",x)
    if '0'==x:
        return 0.
    try:
        valGb=-1.
        if 'g' in x:
            valGb=float(x[:-1])
        elif 'm' in x:
            valGb=float(x[:-1])/1024.
        elif 'k' in x:
            valGb=float(x[:-1])/1024./1024.
        else:
            valGb=float(x)//1024./1024./1024.
    except:
        print("failed memStr2Gb x0=",x0)
        valGb='nan'
    return valGb
#print("test  memStr2Gb=", memStr2Gb('12.3K'))


#------------------------------
def  printNiceTable(perNode):
    keyL=['jobs','users', 'cpu','mem', 'gpu']
    
    nameL=sorted(perNode.keys())

    nodeIdle=[]
    nodeLight=[]
    cnt={x:0 for x in keyL}
    print('\nAllocated TRES on cori-gpu')
    for x in keyL:
        print(' %4s  '%x,end='')
    print(' node')
        
    for node in nameL:        
        oneTr=perNode[node]['allocTres']
        oneJb=perNode[node]['jobs']
        #print('one',one)
        for x in keyL:
            val=0
            if x in ['cpu', 'gpu','mem']:
                val=oneTr[x]
            if x=='jobs': val= len( oneJb)
            if x=='users':
                # construct user list
                userL=[ job['user'] for  job in oneJb]
                val= len( userL)
            cnt[x]+=val
            print('%4d   '%val,end='')
        tag=''
        if  oneTr['gpu']==0:
            nodeIdle.append(node)
            tag=' i'
        elif oneTr['gpu']<3 :
            nodeLight.append(node)
            tag=' +'
        
        print(' '+node+tag)

    print('Total')
    cnt['users']=-1
    for x in keyL:
        print(' %4d  '%cnt[x],end='')
    print('\n')
    print('idle(i)  %d :'%len(nodeIdle),' '.join(nodeIdle))
    print('light(+) %d :'%len(nodeLight),' '.join(nodeLight))
    print('\n')
#...!...!..................
def parseOneNodeTres(line):
    tresL=['cpu','mem','gpu']
    out1={}
    #print(line)
    if len(line) <15:
        for trn in tresL: out1[trn]=0
        out1['idle']=True
        return out1
    if 'gpu' not in line:
        line+=',gres/gpu=8'
        print('Warn  CPU-only job:',line)
    xL=line.split(',')
    #print(xL)
    for x in xL:
        if 'gres/gpu:v100' in x: continue # this is duplicate for gpus
        for trn in tresL:
            if trn not in x: continue
            out1[trn]=x.split('=')[-1]
    out1['mem']=memStr2Gb(out1['mem'])
    
    out1={trn:float(out1[trn]) for trn in tresL}
    #pprint(out1) #allocTres
    return out1
    
#...!...!..................
def parseOneNodeJobs(line,allD):
    # content: JOBID PARTITION   NAME   USER ST  TIME  numNODES NODELIST(REASON)
    #            0    1           2      3    4   5     6     7
    xL=line.split()
    node_name=[xL[7]]
    num_node=int(xL[6])
    
    #print('nnn',node_name, num_node)
    if num_node>1 : # decode compressed node names to plain list
        cmd='scontrol show hostname '+node_name[0]
        task = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        data = task.stdout.read()
        assert task.wait() == 0
        #print('dataByte=',data)
        data=data.decode('utf-8')
        #print('dataByte=',data)
        node_name=data.split('\n')[:-1]
        #print(node_name)
    out1={'jobid':xL[0],'user':xL[3],'num_node':num_node}
    for name in node_name:
        allD[name].append(out1)
    

#...!...!..................
def scanNodesTres(nodeL):
    outD={}
    for name in nodeL:
        cmd='scontrol show node '+name
        task = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        data = task.stdout.read()
        assert task.wait() == 0
        #print('dataByte=',data)
        data=data.decode('utf-8')
        dataL=data.split('\n')
        #print(dataL)
        for line in dataL:
            if 'AllocTRES' in line:
                outD[name]={'allocTres': parseOneNodeTres(line)}
        
    return outD

#...!...!..................
def scanNodesJobs(nodeL):
    cmd='squeue --nodelist '+','.join(nodeL)
    #print('cmd=',cmd)
    outD={}
    task = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    data = task.stdout.read()
    assert task.wait() == 0
    #print('dataByte=',data)
    data=data.decode('utf-8')
    dataL=data.split('\n')
    #print(dataL[0])
    allD={ x:[] for x in nodeL}
    for line in dataL[1:-1]:
        #print('jj',line.split())
        parseOneNodeJobs(line,allD)

    return allD

#...!...!..................
def checkEnv():
    #print('checkEnv():')
    task = subprocess.Popen("sinfo |grep gpu", shell=True, stdout=subprocess.PIPE)
    data = task.stdout.read()
    #print(task.wait(),'dataByte=')
    if task.wait() != 0 :
        print('gpu not seen, do   module load esslurm ')
        exit(0)
    return
   


#=================================
#=================================
#  M A I N 
#=================================
#=================================
args=get_parser()
checkEnv()
nodeL=['cgpu%02d'%i for i in range(1,19)]

#print('monitor %d nodes'%len(nodeL),nodeL)

if args.perNode:
    perNode=scanNodesTres(nodeL)
    #pprint(perNodeInfo)
    perNodeJobs=scanNodesJobs(nodeL)

if args.verb>1:
    print('all jobs by node')
    pprint(perNodeJobs)

# ageragte
for name in perNodeJobs:
    perNode[name]['jobs']=perNodeJobs[name]

dateStop=datetime.datetime.now()
dateNowStr=dateStop.strftime("%Y-%m-%d_%H.%M")
print("\ndate %s "%dateNowStr)
printNiceTable(perNode)
   
