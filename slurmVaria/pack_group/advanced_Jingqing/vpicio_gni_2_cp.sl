#!/bin/bash -l 

#SBATCH -A m2621
#SBATCH -L SCRATCH
#SBATCH -t 00:05:00
#SBATCH -J vpicio_gni1_test
#SBATCH -o vpicio_gni1_test.%j.out 
#SBATCH -e vpicio_gni1_test.%j.err
#SBATCH -p regular
#SBATCH -N 1
#SBATCH -C haswell -J packA
#SBATCH packjob
#SBATCH -p regular
#SBATCH -N 1
#SBATCH -C haswell -J packB

sleep 20
srun --pack-group=0 -n 1 gni_server.sh &

sleep 20
srun --pack-group=1 -n 1 gni_client.sh

export MPICH_GNI_NDREG_MAX_ENTRIES=1024
export HG_TRANSPORT=ofi+gni
export NCLIENT=31
export PDC_DRC_KEY=`cat $SCRATCH/drc.txt`

########### Data location (BB or Lustre) ############
export PDC_TMPDIR="./pdc_hdf5_tmp_gni1"
export PDC_DATA_LOC="$SCRATCH/pdc_hdf5_data_gni1"
echo $PDC_DATA_LOC
mkdir -p $PDC_DATA_LOC/pdc_data

###########        Program Config       ############
N_NODE=1
NCLIENT=31

let TOTALPROC=$NCLIENT*$N_NODE

srun --pack-group=0 --label -n $N_NODE -c 2 /global/homes/e/emu/cleanup/pdc/src/build_gni/bin/pdc_server.exe &
sleep 5
srun --pack-group=1 --label -n $TOTALPROC -c 2 /global/homes/e/emu/cleanup/pdc/src/build_gni/bin/vpicio
echo 'vpicio done'
sleep 5
srun --pack-group=1 --label -n 1 /global/homes/e/emu/cleanup/pdc/src/build_gni/bin/close_server

echo Main END
date
