#!/bin/bash 
sleep 20
procIdx=${SLURM_PROCID}
echo 'client_pack procId=$procIdx started '`date`
echo client procId=$procIdx pack sees nodes $SLURM_NODELIST
if [ $procIdx -eq 0 ]; then
   echo "I am master client"
fi

sleep 20
echo 'client_pack end '`date`
