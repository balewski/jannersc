#!/bin/bash 
procIdx=${SLURM_PROCID}
echo 'server_pack procId=$procIdx started '`date`
echo server procId=$procIdx pack sees nodes $SLURM_NODELIST
if [ $procIdx -eq 0 ]; then
   echo "I am master server"
fi

sleep 40
echo 'server_pack end '`date`
