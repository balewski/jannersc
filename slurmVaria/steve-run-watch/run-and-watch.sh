#!/bin/bash

interval=60
pid=$PPID
logfile=monitor.${SLURM_NODEID:-0}.$(uname -n).$$.out
while [[ $# -gt 0 ]]; do
  case $1 in
    -i) interval=$2 ; shift ;;
    -p) pid=$2 ; shift ;;
    -l) logfile=$2 ; shift ;;
    -x) ex=$2 ; shift ;;
    -s) subject=$2 ; shift ;;
    --) shift ; break ;;
     *) ex=$1 ; shift ; break ;;
  esac
  shift
done

if [[ "${SLURM_LOCALID:-0}" == "0" ]]; then
  echo "sample.sh $interval $logfile $ex "
  ./sample.sh $interval $logfile ${subject:-$ex} &
fi

exec $ex $*

