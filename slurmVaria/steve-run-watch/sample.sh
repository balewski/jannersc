#!/bin/bash
#intended to be called by run-and-watch.sh, so not much flexibility here

pid=$PPID
interval=$1
logfile=$2
ex=${3##*/}

ncores=$(lscpu | awk '/^Thread\(s\) per core/ {tpc=$NF} /^CPU\(s\):/ {cpus=$NF} END {print cpus/tpc}')
cpufreq=lscpu | awk '/^CPU MHz:/ {print $NF}'
awkscr="NR==1 {print \"core \"\$0; next} ; \$11 ~ /^${ex::8}/ { core=int(\$1%$ncores) ; printf(\"%4i %s\n\",core,\$0) }"
echo "watching for $ex on $(uname -n) at $cpufreq MHz at $(date)" >> $logfile
cat /proc/self/cgroup >> $logfile
cat /proc/self/cpuset >> $logfile
echo "" >> $logfile
#which sar  >> $logfile
#/usr/bin/sar -P ALL $interval 2>&1 >> $logfile &
#sarpid=$!


while kill -0 $pid 2>/dev/null ; do
  sleep $interval
  date >> $logfile
  free -g | awk 'NR==2 {tot=$2} NR==3 {free=$4} END {print tot-free "GB in use"}' >> $logfile
  ps H -u $USER -o psr,tid,pid,ppid,pgid,nlwp,pcpu,time,rss,size,comm --sort=pid,tid | awk "$awkscr" >> $logfile
  echo "" >> $logfile
done
#kill $sarpid
