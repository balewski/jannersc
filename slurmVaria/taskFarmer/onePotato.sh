#!/bin/bash

# purpose is run Potato short job
inpA=$1 
inpB=$2
sleepT=30
# note, all echo commands will go to .out, the file is written in to working dir
echo "inPotato-${inpA}-${inpB}-sleep $sleepT "`hostname`"  "`date`
sleep $sleepT
top ibn1 > potato-${inpA}-${inpB}.out
free -g > potato-${inpA}-${inpB}.out
echo janPotato-host-`hostname -f`
echo janPotato-ip-`/sbin/ifconfig |grep 'inet addr:10'`
echo endPotato-${inpA}-${inpB} SKEW=$SKEW 
date

