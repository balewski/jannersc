#!/bin/bash
# purpose is run short tomato job
inpA=$1
inpB=$2
sleepT=39
# note, all echo commands will go to .out, the file is written in to working dir
echo inTomato-${inpA}-${inpB}-sleep $sleepT
date
sleep $sleepT
top ibn1 > tomato-${inpA}-${inpB}.out
echo inTomato-${inpA}-${inpB}-end
date

