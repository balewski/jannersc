#!/bin/bash
core=$1
logF=$WRK_DIR/carot-${core}.log
echo START of core=${core} pwd=`pwd` wrk=$WRK_DIR log=$logF
mkdir $WRK_DIR/${core}
echo inPotato-${core}-host-`hostname -f`
cd $WRK_DIR/${core}

# copy input file from some fixed location
#inpPath=/global/u2/m/mmiah0/gndmotion/asciidata/
#cp ${inpPath}${core}/input .

pwd> $logF
ls -l >> $logF
echo 'start xnevada on '`date` >> $logF

# this is fake job produceing one output out.jan
hostname -f > out.jan
date >>out.jan
top ibn1 >>out.jan
free -g >>out.jan
sleep 30

echo ' end xnevada on '`date` >> $logF
ls -l >> $logF
echo end-of-core-${core}-`date`

