#!/bin/bash 
nEve=200
#nEve=-1

echo run on CRAY nEve=$nEve
whoami

echo start-B
env|grep  SHIFTER_RUNTIME
echo step1-B
whoami

cd /home/balewski/nuwa-docker/NuWa-trunk/
source setup.sh
cd dybgaudi/DybRelease/cmt
source setup.sh

echo testing DYB setup in
cd $SCRATCH/out1
pwd
cat /global/homes/b/balewski/.my.cnf
# note, on Cori you need to use IP for DB: 131.243.2.40

# simple environment test
root -b -q

#  the job
nEve=100
outName=rec.jan.root
inpName=/global/project/projectdirs/dayabay/data/exp/dayabay/2015/daq/Neutrino/1114/daq.Neutrino.0057046.Physics.EH3-Merged.SFO-3._0144.data

date
time nuwa.py --random=off --repack-rpc=on -n $nEve --dbirollback="* = 2015-08-31 00:00:00" @$P14BROOT/share/runReco @$P14BROOT/share/runTags @$P14BROOT/share/runODM @$P14BROOT/share/runFilters -o $outName  $inpName

date

echo end-B nEve=${nEve}
pwd
ls -lrth recon*
