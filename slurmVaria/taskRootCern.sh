#!/bin/bash 

echo start-B
env|grep  SHIFTER_RUNTIME
echo step1-B
whoami

echo count avaliable cores
cat /proc/cpuinfo  |grep processor|nl|tail -n 1

echo show load and RAM on the node
top ibn1

echo setup Cern libs
ls -l ~/0x/setupPhys.sh
source ~/0x/setupPhys.sh

echo  change working dir 
cd $SCRATCH
pwd

echo  simple environment test
root -b -q

echo end-B 
pwd
ls -lrth 
