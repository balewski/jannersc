#!/bin/bash
set -u ;  # exit  if you try to use an uninitialized variable
set -e ;    #  bash exits if any statement returns a non-true return value
set -o errexit ;  # exit if any statement returns a non-true return value
 
totSec=$1
phase=$2
jobName=$3
function myHead {
  echo  -n '{"date":"'`date`'","nodeId":"'`hostname`'","totSec":'$totSec',"phase":'$phase
}


function myLoad {
    nExe=`pgrep $jobName |wc -l`
    echo -n ',"nExe":'$nExe

    # 1-minutes load
    w1=`w|head -1 | cut -f4 -d \, | cut -f2 -d:`
    echo -n ',"loadW1":'$w1
} 

function myRAM {
    # measure RAM
    line=`free -g |grep Mem`
    ramT=`echo $line | cut -f2 -d\ `

    line=`free -g |grep buffers/`
    freeT=`echo $line | cut -f4 -d\ `

    line=`free -g |grep Swap`
    swapT=`echo $line | cut -f2 -d\ `
    swapF=`echo $line | cut -f4 -d\ `

    #echo $ramT, $freeT, $swapT, $swapF
    echo -n ',"ramTot":'$ramT
    echo -n ',"freeTot":'$freeT
    echo -n ',"swapTot":'$swapT
    echo -n ',"swapFree":'$swapF
}

function myBack {
  echo '}'
} 


myHead
myLoad
myRAM
myBack

