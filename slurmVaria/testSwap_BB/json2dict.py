#!/usr/bin/env python
# in C++ use :  printf("{\"core\":\"b\"}\n");

import json
from pprint import pprint

#  h='"core":"b"'   bad: missing brackets
# h="{'core':'b'}"   bad  parethesis

h='{"core":111.22}' 
print ( type(h),h)
d1 = json.loads(h)
print ( type(d1),d1)

h = '{"foo":"bar", "foo2":123, "ww":{"aa":7,"bb":8}}'
d2 = json.loads(h)
# pretty-print
print(json.dumps(d2,sort_keys=True,indent=2))
pprint(d2)


jsonF='aa.json'
print ("read JSON from disc1")
with open(jsonF) as inpFile:
    for line in inpFile:
        print (line)
        d = json.loads(line)
        pprint(d)
    inpFile.close
