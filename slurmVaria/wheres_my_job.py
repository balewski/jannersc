#!/usr/bin/env python

# just while debugging:
from __future__ import print_function
import sys
def debug(s):
    pass
    #print(s, file=sys.stderr)

import shlex

import os
import subprocess
import collections
import re
from operator import mul

def records(s, sep='\n\n'):
    """ yield one record at a time from a line-separated string """
    while True:
        s = s.partition(sep)
        yield s[0]
        if s[2] == '': break
        s = s[2]

class SlurmCluster:
    """ discovers and reports on jobs, nodes, etc managed by SLURM """

    def __init__(self):
        # state of cluster:
        self.nodes = {}  # nodename: dict_of_node_state_and_info
        self.jobs = {}   # jobid: dict of info about job and its steps
        self.queues = {} # name: dict of queue/partition info
        self.rsvs = {}   # name: dict of reservation info
        # derived info for reports:
        self.running_jobs = set() # ids for subset of jobs currently occupying nodes
        #self.users = {}     # username: dict 
        #self.accounts = {}  # account: dict 

    def parse_nodelist(self, nlist_as_text):
        """ generator to translate slurm nodelists like nid0[1-15,17,18] into node names """ 
        prefix, sep, list = nlist_as_text.partition('[')
        if sep:
            for member in list.rstrip(']').split(','):
                first,sep1,last = member.partition('-')
                if sep1:
                    for i in range(int(first),int(last)):
                        yield prefix + str(i)
                else:
                    yield prefix + first
        else:
            yield prefix

    def read_state(self):
        """ call slurm to find nodes and jobs on cluster (expensive, don't do too often) """

        # use 'seconds since epoch' for time values:
        env = os.environ
        env['SLURM_TIME_FORMAT']='%s'

        scontrol = subprocess.Popen('scontrol -a -o show node'.split(), env=env, 
                                    stdout=subprocess.PIPE)
        nodereport,err = scontrol.communicate()

        # currently-running jobs
        # scontrol job report is a bit of an un-parsable mess, even with -o it sometimes crosses lines,
        # and spaces in user directory names etc are not delimited. Skipping -o, we'll parse it carefully
        scontrol = subprocess.Popen('scontrol -a show job'.split(), env=env, stdout=subprocess.PIPE)
        jobreport,err = scontrol.communicate()

        # queued, in-progress and completed-since-midnight jobs and job steps:
        sacct_fields = ['JobID','JobName','QOS','Partition','User','Account','State','Timelimit',
                      'Eligible','Start','Elapsed','End','ExitCode','DerivedExitCode','ReqNodes',
                      'AllocNodes','ReqCPUS','AllocCPUS','NTasks','MinCPU','AveCPU','TotalCPU',
                      'ReqMem','AveRSS','MaxRSS','Layout','Nodelist']
        sacct_cmd = 'sacct -a -p -n -o'.split() + [','.join(sacct_fields)]
        sacct = subprocess.Popen(sacct_cmd, env=env, stdout=subprocess.PIPE)
        sacct_report,err = sacct.communicate()

        scontrol = subprocess.Popen('scontrol -a -o show part'.split(), stdout=subprocess.PIPE)
        qreport,err = scontrol.communicate()

        scontrol = subprocess.Popen('scontrol -a -o show res'.split(), stdout=subprocess.PIPE)
        rsvreport,err = scontrol.communicate()

        for r in nodereport.splitlines():
            # Handle spaces in optional final field "Reason" (for eg DOWN state):
            r0,r1,r2 = r.partition(' Reason=')
            d = dict(f.split('=',1) for f in r0.split())
            if r2: 
                d['Reason'] = r2
            self.nodes[d['NodeName']] = d

        multiplier = {'K': 1, 'M': 1024, 'G': 1024*1024}
        for r in sacct_report.splitlines():
            d = dict(zip(sacct_fields, r.split('|')))
            job,dot,step = d['JobID'].partition('.')
            #debug(str((job,dot,step)))
            if step:
                # accumulate step info in already-collected job record:
                self.jobs[job]['nsteps']+=1
                if d['State'] == 'COMPLETED':
                    self.jobs[job]['ncomplete']+=1
                    self.jobs[job]['nodes_used'].update(self.parse_nodelist(d['Nodelist']))
                elif d['State'] in ('RUNNING','COMPLETING'):
                    self.jobs[job]['nrunning']+=1
                    self.jobs[job]['nodes_in_use'].update(self.parse_nodelist(d['Nodelist']))
                    self.running_jobs.add(job)
                    #self.debug('got running job ' + str(job))
                elif d['State'] in ('PENDING','CONFIGURING'):
                    self.jobs[job]['npending']+=1
                else: # BOOT_FAIL,CANCELLED,FAILED,NODE_FAIL,TIMEOUT,PREEMPTED, or SUSPENDED
                    self.jobs[job]['naborted']+=1
                
                try:
                    for f in 'MaxRSS', 'AveRSS':
                        if d[f] not in ('0',''):
                            v = float(d[f][:-1]) * multiplier[d[f][-1]]
                            jv = self.jobs[job][f]
                            self.jobs[job][f] = max(v, jv)
                except KeyError:
                    debug(str(r))
                    debug(str(d))
                    raise
            else: # job
                d['nsteps'] = 0
                d['ncomplete'] = 0
                d['nrunning'] = 0
                d['npending'] = 0
                d['naborted'] = 0
                d['nodes_allocated'] = set(self.parse_nodelist(d['Nodelist'])) 
                d['nodes_used'] = set()      # list of nodes actually used by past steps
                d['nodes_in_use'] = set()    # list of nodes in use right now by running job steps
                self.jobs[job] = d

        # carefully parse scontrol job report
        # we are only interested in those fields which add info about pending jobs we already 
        # collected from sacct, so we will parse it a bit lazily:
        special_fields = re.compile('\n   (Command|WorkDir|StdErr|StdIn|StdOut|BurstBuffer|Power)=')
        i0 = 0 # start of job record
        while True:
            i1 = jobreport.find('\nJobId=',i0) # end of job record
            if i1==i0: break # finished
            r = special_fields.split(jobreport[i0:i1])
            i0=i1

            d=dict(f.split('=',1) for f in r[0].split())
            job = d['JobId']
            # occasionally a newly-started job is shown by scontrol but not sacct, skip these:
            if not self.jobs.has_key(job): continue
            # the remaining list is key,value,key,value,... up to Power, about which we care not
            d.update(zip(r[1::2],r[2::2]))

            # collect the fields we care about:
            self.jobs[job]['Priority'] = d.get('Priority')
            self.jobs[job]['Reason'] = d.get('Reason')
            self.jobs[job]['Dependency'] = d.get('Dependency')
            self.jobs[job]['SchedNodeList'] = d.get('SchedNodeList')
            self.jobs[job]['Start'] = d.get('StartTime') # scontrol calls "Start" "StartTime"
            self.jobs[job]['BurstBuffer'] = d.get('BurstBuffer')


        for r in qreport.splitlines():
            d = dict(f.split('=',1) for f in shlex.split(r))
            self.queues[d['PartitionName']] = d

        # if there are no reservations (often the case), scontrol prints a message to that effect:
        if not rsvreport.startswith("No reservations"):
            for r in rsvreport.splitlines():
                d = dict(f.split('=',1) for f in shlex.split(r))
                self.rsvs[d['ReservationName']] = d


class ClusterMap:
    """ how to draw a topologically-relevant diagram of the cluster state on a curses pad """

    def __init__(self, slurmcluster, extents=[]):
        self.slurmcluster = slurmcluster
        self.extents = extents
        # the cluster map manages its own curses pad
        self.pad_shape = (0,0)  # not set yet
        self.pad = None
        # cache a sorted list of node names
        self.nodelist = []

        # for drawing the map, each user/account/reservation etc gets a symbol:
        # (strip end of string marker)
        self.user_symbol_list = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'

        # possible node states to be drawn correlate to color pairs:
        next = 0
        states = ['IDLE', 'DOWN', 'ERROR', 'MIXED', 'UNKNOWN']
        self.condition = dict(zip(states[next:], range(next, len(states))))
        symbols = ['.', '-', '#', '&', '*']
        self.condition_symbols = dict(zip( (self.condition[s] for s in states), symbols))
        # white-on-black is default and predefined pair 0
        # curses.init_pair(self.condition['IDLE'], curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(self.condition['DOWN'],   curses.COLOR_RED,   curses.COLOR_BLACK)
        curses.init_pair(self.condition['ERROR'],  curses.COLOR_RED,   curses.COLOR_BLACK)
        curses.init_pair(self.condition['MIXED'], curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(self.condition['UNKNOWN'], curses.COLOR_WHITE, curses.COLOR_BLACK)

        # usage view: fg indicates CPUload, bg indicates mem usage, relative to allocation
        next = len(states)
        states += ['BALANCED']   # not fully allocated but otherwise no concerns
        self.usage_states_matrix = [ [ 'IDLE_EMPTY', 'IDLE_FULL', 'IDLE_VFULL' ],
                                     [ 'BUSY_EMPTY', 'BUSY_FULL', 'BUSY_VFULL' ],
                                     [ 'VBUSY_EMPTY', 'VBUSY_FULL', 'VBUSY_VFULL' ],
                                     [ 'HIGHLIGHT', 'NOT_MY_JOB' ]]
        states += [ s for sublist in self.usage_states_matrix for s in sublist ]
        self.usage = dict(zip(states[next:], range(next, len(states))))
        # cyan is a horrible background color .. maybe rethink coloring! (just black and
        # blue for good and not-so-good, and colors in foreground)
        curses.init_pair(self.usage['BALANCED'],    curses.COLOR_WHITE,  curses.COLOR_BLACK)
        curses.init_pair(self.usage['IDLE_EMPTY'],  curses.COLOR_RED,    curses.COLOR_BLUE)
        curses.init_pair(self.usage['IDLE_FULL'],   curses.COLOR_RED,    curses.COLOR_BLACK)
        curses.init_pair(self.usage['IDLE_VFULL'],  curses.COLOR_RED,    curses.COLOR_BLUE)
        curses.init_pair(self.usage['BUSY_EMPTY'],  curses.COLOR_GREEN,  curses.COLOR_BLUE)
        curses.init_pair(self.usage['BUSY_FULL'],   curses.COLOR_GREEN,  curses.COLOR_BLACK)
        curses.init_pair(self.usage['BUSY_VFULL'],  curses.COLOR_GREEN,  curses.COLOR_BLUE)
        curses.init_pair(self.usage['VBUSY_EMPTY'], curses.COLOR_YELLOW, curses.COLOR_BLUE)
        curses.init_pair(self.usage['VBUSY_FULL'],  curses.COLOR_YELLOW, curses.COLOR_BLACK)
        curses.init_pair(self.usage['VBUSY_VFULL'], curses.COLOR_YELLOW, curses.COLOR_BLUE)
        curses.init_pair(self.usage['NOT_MY_JOB'], curses.COLOR_WHITE, curses.COLOR_BLACK)
        curses.init_pair(self.usage['HIGHLIGHT'], curses.COLOR_BLACK, curses.COLOR_YELLOW)
        # for deciding what state a node is in:
        self.cpu_thresholds = [ 0.5, 1.2 ]
        self.mem_thresholds = [ 0.4, 0.9 ]

        # availability view: fg indicates time till free/since busy, bg indicates whether this 
        # job/partition/reservation can use this node
        next = len(states)
        self.availability_states_matrix = [ 
            ['NOW_ALLOWED', 'VSHORT_ALLOWED', 'SHORT_ALLOWED', 'LONG_ALLOWED', 'VLONG_ALLOWED'],
            ['NOW_DENIED', 'VSHORT_DENIED', 'SHORT_DENIED', 'LONG_DENIED', 'VLONG_DENIED'] ]
        states += [ s for sublist in self.availability_states_matrix for s in sublist ]
        self.availability = dict(zip(states[next:], range(next, len(states))))
        curses.init_pair(self.availability['NOW_ALLOWED'],    curses.COLOR_GREEN,   curses.COLOR_BLACK)
        curses.init_pair(self.availability['VSHORT_ALLOWED'], curses.COLOR_WHITE,   curses.COLOR_BLACK)
        curses.init_pair(self.availability['LONG_ALLOWED'],   curses.COLOR_MAGENTA, curses.COLOR_BLACK)
        curses.init_pair(self.availability['SHORT_ALLOWED'],  curses.COLOR_YELLOW,  curses.COLOR_BLACK)
        curses.init_pair(self.availability['VLONG_ALLOWED'],  curses.COLOR_RED,     curses.COLOR_BLACK)
        curses.init_pair(self.availability['NOW_DENIED'],    curses.COLOR_GREEN,   curses.COLOR_BLUE)
        curses.init_pair(self.availability['VSHORT_DENIED'], curses.COLOR_WHITE,   curses.COLOR_BLUE)
        curses.init_pair(self.availability['SHORT_DENIED'],  curses.COLOR_YELLOW,  curses.COLOR_BLUE)
        curses.init_pair(self.availability['LONG_DENIED'],   curses.COLOR_MAGENTA, curses.COLOR_BLUE)
        curses.init_pair(self.availability['VLONG_DENIED'],  curses.COLOR_RED,     curses.COLOR_BLUE)
        # for deciding what availability a node has:
        self.time_thresholds = [ 60, 300, 3600, 86400 ]

        # and we compute the usage of each node upon request, after an update:
        self.node_usage = {} 
        self.users = {}    # username: dict including symbol
        self.accounts = {} # account: dict including symbol

        self.update()  # initial reading of state

    def node_location(self, node_name):
        """ given a node name, return its location as 4-tuple of position within each rank """
        return (0,0,0,0)

    def node_name(self, location):
        """ given a location as 4-tuple, return the node name """
        return ''

    def node_label(self, location):
        """ given a location as a 4-tuple, return the node label """
        return ''

    def node_marker_location(self, nodename):
        """ where on the pad to draw this node (returns a tuple) """
        return (0,0)

    def update(self):
        self.slurmcluster.read_state()
        self.nodelist = self.slurmcluster.nodes.keys()
        self.nodelist.sort()
        debug('location of first node is ' + str(self.node_location(self.nodelist[0])))
        debug('location of last node is ' + str(self.node_location(self.nodelist[-1])))
        self.update_node_usage()

    def draw_frame(self, width_hint=0):
        """ ensure suitable pad to draw map of cluster described by slurmcluster.nodes """
        # first make sure we have a pad of suitable size for the map and,
        # if a hint was given, the screen:
        # map will consist of one or more columns of groups (ie rank-3 networks)
        # column must be wide enough to draw label and rank 0 (nodes) and 1 (slots)
        lastnode_location = self.node_location(self.nodelist[-1])
        label_width = len(self.node_label(lastnode_location))
        # leave a gap between rank-0 blocks
        col_width = label_width + (self.extents[0]+1)*self.extents[1]
        # allow for an (arbitrary) 10-char gap between columns:
        max_cols = max(1, width_hint / (col_width + 10))
        pad_width = col_width*max_cols + 6*(max_cols-1)
        # 1 line for slot labels, plus a line gap before each group:
        ngroups = lastnode_location[3] + 1
        debug('ngroups is ' + str(ngroups))
        # each column has at least ngroups/max_cols groups:
        groups_by_col = [ ngroups / max_cols ] * max_cols
        # ..and additional groups are added to first n columns:
        for i in range(ngroups - max_cols*groups_by_col[-1]):
            groups_by_col[i]+=1
        pad_height = 1 + (self.extents[2]+1)*groups_by_col[0]
        debug('from width_hint ' + str(width_hint) + ' and col_width ' + str(col_width) + ' got max_cols ' + str(max_cols) + ' and pad_hieght ' + str(pad_height))

        if not self.pad or self.pad_shape != (pad_height,pad_width):
            self.pad_shape = (pad_height,pad_width)
            self.pad = curses.newpad(pad_height,pad_width) 

        # y,x on pad for first node in each group (list of tuples)
        self.groupyx = []

        # now draw the labels:
        group = 0
        #debug(str(groups_by_col))
        for col in range(max_cols):
            #debug('labelling col ' + str(col))
            y=0 ; x=col*col_width
            # slot labels (horizontal axis):
            s = '{0:^{width}s}'.format('slot:', width=label_width)
            self.pad.addstr(y,x,s, curses.A_BOLD)
            for slot in range(self.extents[1]):
                s = '{0:^{width}d}'.format(slot, width=self.extents[0]+1)
                self.pad.addstr(s, curses.A_BOLD)

            # rank-1 labels (vertical axis):
            while group < sum(groups_by_col[:col+1]):
                #debug('labelling group ' + str(group))
                y+=1 # gap above each group
                self.groupyx.append( (y, x+label_width) )
                for cage in range(self.extents[2]):
                    label = self.node_label([0,0,cage,group])
                    self.pad.addstr(y,x,label, curses.A_BOLD)
                    y+=1
                group+=1

    def update_node_usage(self):
        self.node_usage = {}  # nodename: dict of usage (job, user, account, usage_state)
        nusers = len(self.users)
        naccounts = len(self.accounts)
        #nsymbols = len(self.user_symbol_list)+1
        nsymbols = len(self.user_symbol_list)
        for job in self.slurmcluster.running_jobs:
            debug('checking job ' + str(job))
            u = self.slurmcluster.jobs[job]['User']
            a = self.slurmcluster.jobs[job]['Account']
            if not u in self.users:
                self.users[u] = {}
                # what and how many jobs does this user have?
                self.users[u]['jobs'] = set()
                try:
                    self.users[u]['symbol'] = self.user_symbol_list[nusers % nsymbols]
                except:
                    # bruenn  62  63  62  62
                    # chaincy  61  62  61  61
                    debug(u + '  ' + str(nusers) + '  ' + str(nsymbols) + '  ' + str(nusers % nsymbols) + '  ' + str(len(self.user_symbol_list)))
                    debug(self.user_symbol_list)
                    raise
                nusers += 1
            self.users[u]['jobs'].add(job)
            if not a in self.accounts:
                self.accounts[a] = {}
                self.accounts[a]['symbol'] = self.user_symbol_list[naccounts % nsymbols]
                naccounts += 1
            for nodename in self.slurmcluster.jobs[job]['nodes_allocated']:
                if nodename in self.node_usage:
                    # already marked as in use, so must have multiple users/jobs
                    debug('marking node ' + nodename + ' as MIXED')
                    self.node_usage[nodename]['condition'] = self.condition['MIXED']
                    if self.node_usage[nodename]['user'] != u:
                        self.node_usage[nodename]['user'] = None
                        #self.node_usage[nodename]['userlist'] += [u]
                        #self.node_usage[nodename]['user_symbol'] = self.symbol_multiple
                    if self.node_usage[nodename]['account'] != a:
                        self.node_usage[nodename]['account'] = None
                        #self.node_usage[nodename]['accountlist'] += [a]
                        #self.node_usage[nodename]['account_symbol'] = self.symbol_multiple
                    self.node_usage[nodename]['job'] = None
                    #self.node_usage[nodename]['joblist'] += [job]
                else:
                    self.node_usage[nodename] = {}
                    self.node_usage[nodename]['user'] = u
                    debug('marking node ' + nodename + ' as user ' + u + ' with symbol ' + self.users[u]['symbol'])
                    #self.node_usage[nodename]['userlist'] = [u]
                    #self.node_usage[nodename]['user_symbol'] = self.users[u]['symbol']
                    self.node_usage[nodename]['account'] = a
                    self.node_usage[nodename]['job'] = job
                    #self.node_usage[nodename]['accountlist'] = [a]
                    #self.node_usage[nodename]['account_symbol'] = self.accounts[u]['symbol']

        for nodename,node in self.slurmcluster.nodes.iteritems():
            if not self.node_usage.has_key(nodename):
                self.node_usage[nodename] = {}

            if node['State'][:4] in ('IDLE','DOWN'):
                self.node_usage[nodename]['condition'] = self.condition[node['State'][:4]]
                self.node_usage[nodename]['user'] = None
                self.node_usage[nodename]['account'] = None
                # TODO if node state is idle but node is not, should trigger error marker
            elif not self.node_usage[nodename].has_key('user'):
                # occasionally scontrol reports a node as ALLOCATED but sacct does not have
                # the job recorded as running yet, mark it as 'unknown'
                self.node_usage[nodename]['condition'] = self.condition['UNKNOWN']
                self.node_usage[nodename]['user'] = None
                self.node_usage[nodename]['account'] = None
            else: # nodes with running jobs
                try:
                    ncores = int(node['CoresPerSocket']) * int(node['Sockets']) * int (node['Boards'])
                    nthreads = int(node['ThreadsPerCore']) * ncores
                    core_allocation = float(node['CPUAlloc']) / nthreads
                    mem_allocation = float(node['AllocMem'])/float(node['RealMemory'])
                    core_usage = float(node['CPULoad']) / ncores
                    if core_allocation > 0.0:
                        core_usage /= core_allocation
                    mem_usage = 1.0 - float(node['FreeMem'])/float(node['RealMemory'])
                    if mem_allocation > 0.0:
                        mem_usage /= mem_allocation
                except:
                    debug(str(node))
                    raise

                for i in range(len(self.cpu_thresholds)):
                   if core_usage < self.cpu_thresholds[i]: break 
                for j in range(len(self.mem_thresholds)):
                   if mem_usage < self.mem_thresholds[i]: break 
                usage = self.usage_states_matrix[i][j]
                self.node_usage[nodename]['usage'] = self.usage[usage]


    def draw_usage(self, user=None, account=None, job=None):
        """ for each node, add a character to the pad showing its current usage,
            optionally highlighting a specific user, account or job
        """
        if user:
            jobs = {}
            i=0
            nsymbols = len(self.user_symbol_list)
            for j in self.users[user]['jobs']:
                jobs[j] = self.user_symbol_list[i % nsymbols]
                i+=1
        for nodename in self.nodelist:
          try:
            location = self.node_location(nodename)
            groupy,groupx = self.groupyx[location[3]]
            y = groupy + location[2]
            x = groupx + location[1]*(self.extents[0]+1) + location[0]
            nodeuser = self.node_usage[nodename]['user']
            if nodeuser and nodeuser==user:
                usage = self.node_usage[nodename]['usage']
                symbol = jobs[self.node_usage[nodename]['job']]
            elif nodeuser and not user:
                usage = self.node_usage[nodename]['usage']
                symbol = self.users[nodeuser]['symbol']
            elif nodeuser:
                usage = self.usage['NOT_MY_JOB'] 
                symbol = self.users[nodeuser]['symbol']
            else:
                usage = self.node_usage[nodename]['condition']
                symbol = self.condition_symbols[usage]
            if job and self.node_usage[nodename].get('job', None) == job:
                usage = self.usage['HIGHLIGHT']
            attr = curses.color_pair(usage)
            #debug(str((y,x,symbol,attr)))
            self.pad.addch(y,x,symbol,attr)
            #self.pad.addch(y,x,symbol)
            #self.pad.addch(10,20,symbol)
          except:
            debug(nodename)
            debug(str(self.node_usage[nodename]))
            debug(str(self.slurmcluster.nodes[nodename]))
            debug(str(location))
            debug(str(self.groupyx))
            debug(str(y) + "   " + str(x))
            debug(str(y) + ',' + str(x) + ',' + symbol)
            debug(str(usage))
            #debug(str(y) + ',' + str(x) + ',' + symbol + ',' + str(attr))
            #debug(str((usage, attr)))
            raise

    def draw_availability(self):
        # for each node, add a character to the pad showing its current usage
        # TODO...
        pass


class CrayXCMap(ClusterMap):
    """ specifics for XC30/XC40 Aries/Dragonfly cluster """

    def __init__(self, slurmcluster, rows=1, cols_per_row=1):
        # rows and cols are for labelling, which follows convention c#-#c#s#n#
        # where the 4 numbers are col, row, cage, slot, node. For labelling we 
        # only use c#-#c#. 
        #
        # Aries/Dragonfly: rank-0 is 4 nodes per blade, rank-1 is 16 blades
        # per cage, rank-2 is 6 cages per group. Rank-3 is some number of 
        # groups, determined at run time from scontrol output
        # for labelling
        ClusterMap.__init__(self, slurmcluster, [4, 16, 6, 0])
        # a cage encloses a rank-1 network on XC:
        self.nodes_per_cage = reduce(mul, self.extents[:2])
        self.cages_per_cabinet = 3
        # rows and columns of cabinets:
        self.rows = rows
        self.cols_per_row = cols_per_row

    def node_location(self, node_name):
        # Cray node names are form 'nid00000' where the number indicates location
        nid = int(node_name.lstrip('nid'))
        blocksizes = [ self.extents[0] ]
        for i in self.extents[1:]:
            blocksizes.append(blocksizes[-1]*i)
        location = [0]*4
        for i in range(1,len(blocksizes)):
            location[-i] = nid / blocksizes[-i-1]
            nid -= location[-i] * blocksizes[-i-1]
        location[0] = nid % blocksizes[0]
        return location

    def nid(self, location):
        """ calculate the nid (integer) from its 4-tuple location """
        nid = location[3]*reduce(mul, self.extents[:3]) 
        nid += location[2]*reduce(mul, self.extents[:2]) 
        nid += location[1]*self.extents[0] + location[0]
        return nid

    def node_name(self, location, nid=None):
        # at NERSC at least, Cray node names are 'nid' and a 5-wide nid number
        """ given a location as 4-tuple, return the node name """
        if not nid:
            nid = self.nid(location)
        return 'nid{0:05d}'.format(nid)

    def node_label(self, location):
        """ given a location as a 4-tuple, return the node label """
        nid = self.nid(location)
        cage = nid / self.nodes_per_cage
        cabinet = cage / self.cages_per_cabinet
        row = cabinet / self.cols_per_row
        col = cabinet % self.cols_per_row
        cage %= self.cages_per_cabinet
        return '{0} (c{1}-{2}c{3}):'.format(self.node_name(location, nid), col, row, cage)




# curses in a nutshell:
# - coords are (y,x) starting in top-left corner of screen as 0,0
# - the initial window covers the full screen, other windows are created with newwin
# - newwin takes the size and location of the new window
# - a "pad" is a kind of vitual window, defined only by height and width. Displaying
#   a pad takes a point on the pad and a location and size on screen to display the 
#   part of the pad starting at that point. newpad returns a window object.
# curses.wrapper is a utility function which sets up the initial window and environment,
# and upon interrupt or exit, returns the terminal to a sensible state. It takes a 
# function as its argument, which is the curses-using program, and should accept a 
# single argument "stdscr", into which the wrapper will pass the handle for the iniital
# window. 

#from curses import wrapper
import curses
import getopt

def main(stdscr):

    usage = "show jobs on a topologial map of cluster"
    usage += sys.argv[0] + " [-u user] [-j jobid] "
    user=None
    job=None
    try:
        opts, args = getopt.getopt(sys.argv[1:], 'hu:j:', ['help', 'user', 'jobid'])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    for opt in opts:
        if opt[0] in ('-h', '--help'):
            print(usage)
            sys.exit(0)
        elif opt[0] in ('-u', '--user'):
            user = opt[1]
        elif opt[0] in ('-j', '--jobid'):
            job = opt[1]
        else:
            print(usage)
            sys.exit(2)

    stdscr.clear()
    stdscr.refresh()
    term_height, term_width = stdscr.getmaxyx()

    cluster = SlurmCluster()
    map = CrayXCMap(cluster, rows=4, cols_per_row=8)

    map.draw_frame(term_width)
    map.draw_usage(user=user, job=job)
    # noutrefresh args are (poorly documented): pad top,left, then window top,left,height,width
    debug(str((term_height, term_width)))
    map.pad.noutrefresh( 0,0, 0,0, term_height-1, term_width-1 )

    stdscr.refresh()
    curses.doupdate()
    stdscr.getkey()


curses.wrapper(main)
