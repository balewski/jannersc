#!/bin/bash
echo
# line from Kelly showing load on all GPUs

#sacct -a -q gpu,gpu_high,gpu_preempt -o user,JobID,AllocGRES,NNodes,NCPUs,Nodelist,State,JobName  -X |grep RUN |sort|nl

echo -n '       '
#sacct -a -q gpu,gpu_high,gpu_preempt -o user,JobID,AllocGRES,NNodes,NCPUs,Nodelist,State,JobName  -X |head -n1

sacct -a -q  gpu_regular,gpu_special_m1759,resv_shared   -o user,JobID,AllocGRES,NNodes,NCPUs,Nodelist,State,JobName  -X |grep RUN |sort|nl

sacct -a -q gpu_regular,gpu_special_m1759  -o user,JobID,AllocGRES,NNodes,NCPUs,Nodelist,State,JobName  -X |head -n1
sinfo |grep "gpu              up" |grep idle
