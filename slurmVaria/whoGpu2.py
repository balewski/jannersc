#!/usr/bin/env python
""" CoriGPU nodes usage

Steve's command:
squeue -p gpu --Format=jobid:.10,partition:.10,username:.10,statecompact:.3,timeused:.8,numnodes:.6,numcpus:.5,minmemory:.12,tres-per-node:.14,tres-per-job:.13,nodelist:.12
     JOBID PARTITION      USER ST    TIME NODES CPUS  MIN_MEMORY TRES_PER_NODE TRES_PER_JOB    NODELIST
    592973       gpu  sfarrell PD    0:00    16  160       4682M         gpu:8          N/A
    569162       gpu  coughlin PD    0:00     1    1          8G           N/A        gpu:1
    594879       gpu  coughlin  R 1:48:21     1    2          8G           N/A        gpu:1      cgpu07
    594880       gpu  coughlin  R 1:48:21     1    2          8G           N/A        gpu:1      cgpu08


"""

from __future__ import print_function, unicode_literals
from __future__ import absolute_import, division

import sys,os
#import json
from  pprint import pprint
import copy
import datetime
import itertools

import subprocess

__author__ = "Jan Balewski"
__email__ = "balewski@lbl.gov"


def timeStr2sec(st): #------------------
    
    x0=st.split('-')
    #print (st, len(x0 ))    
    tday=0
    if len(x0)==2:
        tday=int(x0[0])*24*3600
        x1 = x0[1].split(':')    
    else:
        x1 = x0[0].split(':')    
    
    t=0.
    try:
        if len(x1)==3:
            t=float(x1[0])*3600. +float(x1[1])*60 +float(x1[2])
        elif len(x1)==2:
            t=float(x1[0])*60 +float(x1[1])
        else:
            t=float(x1[0])
    except:
        #print('failed to convert time, assume 0, inp=',x1,t)
        a=1

    return t+tday
                    
#------------------------------
def  niceNodeTable(dataD):
    keyL=['Rjob', 'Rcpu', 'Rgpu']
    
    nameL=sorted(dataD.keys())
    
    cnt={x:0 for x in keyL}
    for x in keyL:
        print('%4s '%x,end='')
    print('  node')
        
    for node in nameL:
        one=dataD[node]
        for x in keyL:
            val=one[x]
            cnt[x]+=val
            print('%3d  '%val,end='')
        light='*' if one['Rgpu']==1 else ''
        print(' '+node+light)

    print('Total')
    for x in keyL:
        print('%3d '%cnt[x],end='')
    print('')



#------------------------------
def  XagregatePartitionData(byAcct):

    byPart={}
    for x in byAcct:
        #print(x, byAcct[x])
        if 'TOTAL' in x: continue
        partN=x.split()[1]
        if partN not in byPart : byPart[partN]={'Rjob':0, 'Rcpu':0, 'Rcpu*h':0 ,'PDjob':0,'PDcpu':0}
        for y in  byAcct[x]:
            byPart[partN][y]+=byAcct[x][y]
    #print('ppp',byPart)
    return byPart



#------------------------------
def  niceUserTable(dataD):
    keyL=['Rjob', 'Rcpu', 'Rcpu*h', 'PDjob','PDcpu']
    for x in keyL:
        print('%7s  '%x,end='')

    print('    user:account:partition')
    for us in sorted(dataD.keys()):
         for x in keyL:
            #print(x)
            if x=='Rcpu*h': print('%7.1f  '%dataD[us][x],end='')
            else:            print('%7d  '%dataD[us][x],end='')
         print('    %s'%us)

    print('')

#------------------------------
def agregateUserData( allD):
    byUser={}
    byNode={}
    pref='R'
    for job in allD:
        name=job['user']
        if name not in byUser: byUser[name]={'Rjob':0, 'Rcpu':0, 'Rgpu':0, 'Rnodes':set()}
        oneUser=byUser[name]
        #print('kk',oneUser.keys())
        #print('cc0',job['nodes'])
        
        oneUser[pref+'job']+=1
        oneUser[pref+'cpu']+=job['ncpu']
        oneUser[pref+'gpu']+=job['ngpu']
        for name in job['nodes'] :
            #print('cc',name)
            oneUser[pref+'nodes'].add(name)
            if name not in byNode: byNode[name]={pref+'job':0, pref+'cpu':0, pref+'gpu':0}
            oneNode=byNode[name]
            oneNode[pref+'job']+=1
            oneNode[pref+'cpu']+=job['ncpu']
            oneNode[pref+'gpu']+=job['ngpu']
       
        #break
    #print('agr by %d users:'%len(byUser))
    #pprint(byUser)
    #print('agr by %d node:'%len(byNode))
    #pprint(byNode)
    return byUser,byNode    

#...!...!..................
def expand_nodes(inp):
    if '[' not in inp: return inp
    print('inp=',inp)
    s = "1-3,6,8-10"
    i1=inp.find('[')
    i2=inp.find(']')
    print(i1,i2)
    assert i2>i1
    s=inp[i1:i2+1]
    print('s=%s='%s)
    
    print(list(itertools.chain.from_iterable(range(int(ranges[0]), int(ranges[1])+1) for ranges in ((el+[el[0]])[:2] for el in (miniRange.split('-') for miniRange in s.split(','))))))

    crash

#...!...!..................
def checkEnv():
    print('checkEnv():')
    task = subprocess.Popen("sinfo |grep gpu", shell=True, stdout=subprocess.PIPE)
    data = task.stdout.read()
    #print(task.wait(),'dataByte=')
    if task.wait() != 0 :
        print('gpu not seen, do   module load esslurm ')
        exit(0)
    return
    assert task.wait() == 0
    #
    dataStr=data.decode('utf-8')
    #dataStr=data.encode('ascii', 'ignore')
    #print('dataStr=',dataStr)
    if 'slurm' not in dataStr:
        print('aborted, execute first:\n module load slurm')
        exit(3)
    #print('slurm environment is set up')
   

#...!...!..................
def scanSlurmJobs():
    #print('scanSlurmJobs():')
    cmd="sacct -a -q  gpu_regular,gpu_special_m1759,resv_shared   -o user,JobID,AllocGRES,NNodes,NCPUs,Nodelist,State,JobName -P "

    #/opt/esslurm/bin/sacct -r gpu --format=StartTime,job,nodelist,nnodes -X | sort
    # EndTime ,  alloccpus, draw it as a timeline
    
    task = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
    data = task.stdout.read()
    assert task.wait() == 0
    #print('dataByte=',data)
    data=data.decode('utf-8')
    #dataStr=data.encode('ascii', 'ignore')
    
    skipStateL=['CANCELLED','COMPLETED','TIMEOUT','FAILED']
    headL=[]
    n=0

    if 0: # debug on raw dump of : squeue --array
        fd=open('d1','r')
        dataL = fd.readlines()
        print ("Read input len" ,len(dataL),dataL[:4])
        dataStr=''.join(dataL)
        #exit(3)

    outRun=[]
    outPend=[]
    #print('data=',data)
    for line in data.split('\n'):
        if len(line)<2: continue
        if 'launch failed ' in line: continue
        #print('line',line)

        xL=line.split('|')
        if len(headL)==0:
            headL=[ str(x) for x in xL ]
            #print('headL',headL)
            continue
        n+=1
        #print('xx',xL)
        #.....  unpack line w/ data
        assert len(xL)==8        
        user=xL[0]
        jid=xL[1]
        nnode=int(xL[3])
        ncpu=int(xL[4])
        #nodes=expand_nodes(xL[5])
        nodes=xL[5]
        
        state=xL[6]
        jobname=xL[7]
        ngpu=int(xL[2][4:]) if  state =='RUNNING' else 0

        if len(user)<=0 : continue # those are job-segements
        skip=False
        for x in skipStateL:
            #print(state,skip,  x in state)
            skip |= x in state
        if skip  : continue
        rec={'user':user,'ncpu':ncpu,'nnode':nnode}
        
        if 'RUN' in state:
            #print('user=%s='%user,jid,ngpu,nnode,ncpu,nodes,state,jobname, state in skipStateL, state)
            #print('cc',xL[5])
            #ok22
            rec['ngpu']=ngpu
            rec['nodes']=[nodes]
            outRun.append(rec)
        else:
            outPend.append(rec)
            #print(rec)
    return outRun, outPend

################################
#     MAIN
################################

if __name__ == '__main__':
    dateStop=datetime.datetime.now()
    dateNowStr=dateStop.strftime("%Y-%m-%d_%H.%M")
    #print('dump env:',os.environ)

    '''
    print ("\n %s SLURM usage, all PDSF users, ver 1.8\n"%dateNowStr)
    if  len(sys.argv)>1:
        print (" Columns:  Rjob     Rcpu   Rcpu*h    PDjob    PDcpu ")
        print (" R - running,  PD - pending ")
        print (" job - slurm job count, arrays are unrolled")
        print (" cpu - task on node count, one job may lock many tasks")
        print (" cpu*h - sum of CPU*hours used by jobs in execution")
        exit(1)

    '''
    checkEnv()
    allRun,allPend=scanSlurmJobs()
    #print('M: num accnts=',len(allD))
    #pprint(allD)
    byUser,byNode=agregateUserData( allRun)
    print("\n",dateNowStr)
    niceNodeTable(byNode)

    anyNode=['cgpu%02d'%i for i in range(1,19)]
    #print('a',anyNode)
    idleNode=[]
    for x in anyNode:
        if x in byNode: continue
        idleNode.append(x)
    print('\npotential idle %d nodes:'%(len(idleNode)),' '.join(idleNode))
    
'''
copy new version to
ssh -I /Library/OpenSC/lib/opensc-pkcs11.so sg-crt.nersc.gov

ssh root@mc0154-ib.nersc.gov

cd /chos/common/nsg/slurm/17.02.7/bin 
mv slusers.py old-slusers.py

scp -rp balewski@pdsf:janNersc/pdsfVaria/slusers.py .

chmod a+x slusers.py
chmod a+r slusers.py

'''
